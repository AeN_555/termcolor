package termcolor_test

import (
	"fmt"
	"gitlab.com/AeN_555/termcolor"
	"gitlab.com/AeN_555/termcolor/colors16"
	"gitlab.com/AeN_555/termcolor/colors256"
	"gitlab.com/AeN_555/termcolor/colors8"
	"gitlab.com/AeN_555/termcolor/format"
)

// The common usage of the package for 8bits color.
// Here a yellow bold string on blue background.
func Example_8Bits() {
	myStyle := termcolor.Style{
		TextColor:       colors8.Yellow,
		BackgroundColor: colors8.Blue,
		Attributes:      []format.Format{format.Bold},
	}
	fmt.Print(termcolor.String(&myStyle, "on8bits"))
	// raw: ^[[1;33;44mon8bits^[[0m
	// interpreted: on16bits (bold, in yellow on blue background)
}

// The common usage of the package for 16bits color.
func Example_16Bits() {
	myStyle := termcolor.Style{
		TextColor:       colors16.DarkGray,
		BackgroundColor: colors16.LightGreen,
		Attributes:      []format.Format{format.Underline},
	}
	fmt.Print(termcolor.String(&myStyle, "on16bits"))
	// raw: \033[4;90;102mon16bits\033[0m
	// interpreted: on16bits (underlined, in dark gray on light green background)
}

// The common usage of the package for 88bits.
func Example_88Bits() {
	myStyle := termcolor.Style{
		TextColor:       colors256.NewColor(190), // yellow-ish
		BackgroundColor: colors256.NewColor(233), // red-ish
		Attributes:      []format.Format{format.Normal},
	}
	fmt.Print(termcolor.String(&myStyle, "on88bits"))
	// raw: \033[0;38;5;190;48;5;196mon88bits\033[0m
	// interpreted: on88bits (in yellow-ish on red-ish background)
}

// The common usage of the package for 256bits color.
func Example_256Bits() {
	myStyle := termcolor.Style{
		TextColor:       colors256.NewColor(190), // yellow-ish
		BackgroundColor: colors256.NewColor(233), // red-ish
		Attributes:      []format.Format{format.Blink},
	}
	fmt.Print(termcolor.String(&myStyle, "on256bits"))
	// raw: \033[5;38;5;190;48;5;196mon256bits\033[0m
	// interpreted: on256bits (blink in yellow-ish on red-ish background)
}

// The advanced usage of the package.
func Example_mixedAndDynamic() {
	myStyle := termcolor.Style{
		TextColor:       colors8.Red,
		BackgroundColor: colors256.NewColor(245), // gray-ish
		Attributes:      []format.Format{format.Bold, format.Underline},
	}
	fmt.Print(termcolor.String(&myStyle, "mixed", 4, "test"))
	// raw: \033[1;4;31;48;5;245mmixed4test\033[0m
	// interpreted: mixed4test (bold and underlined, in red on gray-ish background)
}

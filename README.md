## TermColor (Terminal Color)

![Logo](./logo.png "TermColor logo")

TermColor is a package for Go programs to help display any colors in a terminal.

![Example of colored terminal](./example.png)

### Requierements

There is no dependencies for this package except [Go](https://golang.org/ "Link to Golang") itself.

### Installation

TermColor use Go module system, so you don't need to install it to be able to use it.
Just add the necessary imports in your import list like in the example below.

### Example

```go
package main

import (
	"gitlab.com/AeN_555/termcolor"
	"gitlab.com/AeN_555/termcolor/colors8"
	"gitlab.com/AeN_555/termcolor/format"
	"fmt"
)

func main() {
	myStyle := colors.Style{
		TextColor:       colors8.Yellow,
		BackgroundColor: colors8.Blue,
		Attributes:      []format.Format{format.Bold},
	}
	fmt.Println("Does-it work ? " + termcolor.String(&myStyle, "Yes") + " or No")
}
```

### Build and result

```bash
$ go build .
$ ./myBinaryWithColoredOutput
```

![Result of the example](./result.png)

### Compatibility warning

TermColor formats strings so it's only an helper (no more, no less).
When you use it, the result will be very dependent of the current terminal and its capabilities.
Here's a recap (not exhaustive) :

Terminal | Bold | Dim | Underlined | Blink | Invert | Hidden | 8 colors | 16 colors | 88 colors | 256 colors
:--- | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---: | :---:
aTerm | ✔️ | ✘ | ✔️ | ✘ | ✔️ | ✘ | ✔️ | ✔️ | ✘ | ✘
Eterm | ✔️ | ✘ | ✔️ | ✘ | ✔️ | ✘ | ✔️ | ✔️ | ✘ | ✔️
GNOME Terminal | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✘ | ✔️
Guake | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✘ | ✔️
Konsole | ✔️ | ✘ | ✔️ | ✔️ | ✔️ | ✘ | ✔️ | ✔️ | ✘ | ✔️
Nautilus Terminal | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✘ | ✔️
rxvt | ✔️ | ✘ | ✔️ | ✔️ | ✔️ | ✘ | ✔️ | ✔️ | ✔️ | ✘
Terminator | ✔️ | ✔️ | ✔️ | ✘ | ✔️ | ✔️ | ✔️ | ✔️ | ✘ | ✔️
Tilda | ✔️ | ✘ | ✔️ | ✔️ | ✔️ | ✘ | ✔️ | ✔️ | ✘ | ✘
XFCE4 Terminal | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✘ | ✔️
XTerm | ✔️ | ✘ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✘ | ✔️
xvt | ✔️ | ✘ | ✔️ | ✘ | ✔️ | ✘ | ✘ | ✘ | ✘ | ✘
Linux TTY | ✔️ | ✘ | ✘ | ✘ | ✔️ | ✘ | ✔️ | ✔️ | ✘ | ✘
VTE Terminal | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✔️ | ✘ | ✔️

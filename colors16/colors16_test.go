package colors16_test

import (
	"gitlab.com/AeN_555/termcolor/colors16"
	"testing"
)

// To test the text color for every 16bits colors.
func TestTextColors(t *testing.T) {
	checkColor(t, "Dark Gray", colors16.DarkGray.TextTag(), "90")
	checkColor(t, "Light Red", colors16.LightRed.TextTag(), "91")
	checkColor(t, "Light Green", colors16.LightGreen.TextTag(), "92")
	checkColor(t, "Light Yellow", colors16.LightYellow.TextTag(), "93")
	checkColor(t, "Light Blue", colors16.LightBlue.TextTag(), "94")
	checkColor(t, "Light Magenta", colors16.LightMagenta.TextTag(), "95")
	checkColor(t, "Light Cyan", colors16.LightCyan.TextTag(), "96")
	checkColor(t, "White", colors16.White.TextTag(), "97")
}

// To test the background color for every 16bits colors.
func TestBackgroundColors(t *testing.T) {
	checkColor(t, "Dark Gray background", colors16.DarkGray.BackgroundTag(), "100")
	checkColor(t, "Light Red background", colors16.LightRed.BackgroundTag(), "101")
	checkColor(t, "Light Green background", colors16.LightGreen.BackgroundTag(), "102")
	checkColor(t, "Light Yellow background", colors16.LightYellow.BackgroundTag(), "103")
	checkColor(t, "Light Blue background", colors16.LightBlue.BackgroundTag(), "104")
	checkColor(t, "Light Magenta background", colors16.LightMagenta.BackgroundTag(), "105")
	checkColor(t, "Light Cyan background", colors16.LightCyan.BackgroundTag(), "106")
	checkColor(t, "White background", colors16.White.BackgroundTag(), "107")
}

// To check a color value and fail if necessary.
func checkColor(t *testing.T, name string, value string, ref string) {
	if value != ref {
		t.Errorf("%s color has wrong value: \"%s\" (!= \"%s\")", name, value, ref)
	}
}

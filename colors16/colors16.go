// Package colors16 provides predefined color values for 16bits colors.
package colors16

import (
	"fmt"
	"gitlab.com/AeN_555/termcolor"
)

type (
	color struct {
		value uint8
	}
)

const (
	backgroundOffset = 10
)

var (
	DarkGray     termcolor.Colorer = &color{value: 90}
	LightRed     termcolor.Colorer = &color{value: 91}
	LightGreen   termcolor.Colorer = &color{value: 92}
	LightYellow  termcolor.Colorer = &color{value: 93}
	LightBlue    termcolor.Colorer = &color{value: 94}
	LightMagenta termcolor.Colorer = &color{value: 95}
	LightCyan    termcolor.Colorer = &color{value: 96}
	White        termcolor.Colorer = &color{value: 97}
)

// To convert a 16bits color to a tag string for text.
func (c *color) TextTag() string {
	return fmt.Sprint(c.value)
}

// To convert a 16bits color to a tag string for background.
func (c *color) BackgroundTag() string {
	return fmt.Sprint(backgroundOffset + c.value)
}

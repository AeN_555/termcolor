package termcolor_test

import (
	"gitlab.com/AeN_555/termcolor"
	"gitlab.com/AeN_555/termcolor/colors16"
	"gitlab.com/AeN_555/termcolor/colors256"
	"gitlab.com/AeN_555/termcolor/colors8"
	"gitlab.com/AeN_555/termcolor/format"
	"testing"
)

// To test some styles.
func TestStyle(t *testing.T) {
	style8 := termcolor.Style{
		TextColor:       colors8.Magenta,
		BackgroundColor: colors8.Default,
		Attributes:      []format.Format{},
	}
	checkStyle(t, "8bits color", &style8, "\033[35;49m")

	style16 := termcolor.Style{
		TextColor:       colors16.LightCyan,
		BackgroundColor: colors16.White,
		Attributes:      []format.Format{},
	}
	checkStyle(t, "16bits color", &style16, "\033[96;107m")

	style256 := termcolor.Style{
		TextColor:       colors256.NewColor(55),
		BackgroundColor: colors256.NewColor(55),
		Attributes:      []format.Format{},
	}
	checkStyle(t, "256bits color", &style256, "\033[38;5;55;48;5;55m")

	styleMixed1Format := termcolor.Style{
		TextColor:       colors256.NewColor(255),
		BackgroundColor: colors8.Cyan,
		Attributes:      []format.Format{format.Bold},
	}
	checkStyle(t, "mixed colors and 1 format", &styleMixed1Format, "\033[1;38;5;255;46m")

	styleMixedMultiFormats := termcolor.Style{
		TextColor:       colors8.Red,
		BackgroundColor: colors256.NewColor(7),
		Attributes:      []format.Format{format.Blink, format.Reverse, format.Strikethrough},
	}
	checkStyle(t, "mixed colors and multiple formats", &styleMixedMultiFormats, "\033[5;7;9;31;48;5;7m")
}

// To check a style and fail if necessary.
func checkStyle(t *testing.T, name string, style termcolor.Styler, ref string) {
	value := style.TagString()
	if value != ref {
		t.Errorf("%s style has wrong tag string: \"\\033%s\" (!= \"\\033%s\")", name, value[1:], ref[1:])
	}
}

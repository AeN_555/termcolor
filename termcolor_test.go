package termcolor_test

import (
	"gitlab.com/AeN_555/termcolor"
	"gitlab.com/AeN_555/termcolor/colors16"
	"gitlab.com/AeN_555/termcolor/colors256"
	"gitlab.com/AeN_555/termcolor/colors8"
	"gitlab.com/AeN_555/termcolor/format"
	"testing"
)

// To test some styled strings.
func TestStyledStrings(t *testing.T) {
	style8 := termcolor.Style{
		TextColor:       colors8.Yellow,
		BackgroundColor: colors8.Blue,
		Attributes:      []format.Format{format.Bold},
	}
	strStyle8 := termcolor.String(&style8, "on8bits")
	checkStyledString(t, "8bits color", strStyle8, "\033[1;33;44mon8bits\033[0m")

	style16 := termcolor.Style{
		TextColor:       colors16.DarkGray,
		BackgroundColor: colors16.LightGreen,
		Attributes:      []format.Format{format.Underline},
	}
	strStyle16 := termcolor.String(&style16, "on16bits")
	checkStyledString(t, "16bits color", strStyle16, "\033[4;90;102mon16bits\033[0m")

	style88 := termcolor.Style{
		TextColor:       colors256.NewColor(190),
		BackgroundColor: colors256.NewColor(233),
		Attributes:      []format.Format{format.Normal},
	}
	strStyle88 := termcolor.String(&style88, "on88bits")
	checkStyledString(t, "88bits color", strStyle88, "\033[0;38;5;190;48;5;233mon88bits\033[0m")

	style256 := termcolor.Style{
		TextColor:       colors256.NewColor(190),
		BackgroundColor: colors256.NewColor(233),
		Attributes:      []format.Format{format.Blink},
	}
	strStyle256 := termcolor.String(&style256, "on256bits")
	checkStyledString(t, "256bits color", strStyle256, "\033[5;38;5;190;48;5;233mon256bits\033[0m")

	styleMixedDynamic := termcolor.Style{
		TextColor:       colors8.Red,
		BackgroundColor: colors256.NewColor(245),
		Attributes:      []format.Format{format.Bold, format.Underline},
	}
	strStyleMixedDynamic := termcolor.String(&styleMixedDynamic, "mixed", 4, "test")
	checkStyledString(t, "Mixed style with dynamic input", strStyleMixedDynamic, "\033[1;4;31;48;5;245mmixed4test\033[0m")
}

// To check a styled string and fail if necessary.
func checkStyledString(t *testing.T, name string, value string, ref string) {
	if value != ref {
		t.Errorf("%s has wrong value: \"\\033%s\\033[0m\" (!= \"\\033%s\\033[0m\")", name, value[1:len(value)-4], ref[1:len(value)-4])
	}
}

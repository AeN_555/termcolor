// Package termcolor provides terminal colorisation for strings.
package termcolor

import (
	"fmt"
)

const (
	Begin     = "\033["
	End       = "m"
	Separator = ";"

	Reset = Begin + "0" + End
)

type (
	// TextTag() returns the tag to add text style for the following string in a terminal.

	// BackgroundTag() returns the tag to add background style for the following string in a terminal.
	Colorer interface {
		TextTag() string
		BackgroundTag() string
	}
)

// To add style to a message. Message will be concatenated and stylized following the style.
func String(style Styler, message ...interface{}) string {
	return style.TagString() + fmt.Sprint(message...) + Reset
}

package termcolor

import (
	"fmt"
	"gitlab.com/AeN_555/termcolor/format"
)

type (
	// TagString() returns the tag to add style to the following string in a terminal.
	Styler interface {
		TagString() string
	}

	// Style contains every data to create the style tag.
	Style struct {
		TextColor       Colorer
		BackgroundColor Colorer
		Attributes      []format.Format
	}
)

// To convert a style to a terminal's tag string.
func (s *Style) TagString() string {
	res := Begin

	for _, attr := range s.Attributes {
		res = res + fmt.Sprint(attr) + Separator
	}

	return res +
		s.TextColor.TextTag() +
		Separator +
		s.BackgroundColor.BackgroundTag() +
		End
}

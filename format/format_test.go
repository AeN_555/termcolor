package format_test

import (
	"gitlab.com/AeN_555/termcolor/format"
	"testing"
)

// To test every format values.
func TestFormatValue(t *testing.T) {
	checkFormat(t, "Normal", format.Normal, 0)
	checkFormat(t, "Bold", format.Bold, 1)
	checkFormat(t, "Dim", format.Dim, 2)
	checkFormat(t, "Underline", format.Underline, 4)
	checkFormat(t, "Blink", format.Blink, 5)
	checkFormat(t, "Reverse", format.Reverse, 7)
	checkFormat(t, "Hidden", format.Hidden, 8)
	checkFormat(t, "Strikethrough", format.Strikethrough, 9)
}

// To check a format value and fail if necessary.
func checkFormat(t *testing.T, name string, value format.Format, ref uint) {
	if uint(value) != ref {
		t.Errorf("%s format has wrong value: \"%d\" (!= \"%d\")", name, value, ref)
	}
}

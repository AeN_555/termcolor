// Package format provides tags values to format a string in a terminal.
package format

type (
	Format uint
)

const (
	Normal Format = iota
	Bold
	Dim
	_
	Underline
	Blink
	_
	Reverse
	Hidden
	Strikethrough
)

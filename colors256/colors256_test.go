package colors256_test

import (
	"gitlab.com/AeN_555/termcolor/colors256"
	"testing"
)

// To test a blue-ish in 88/256bits colors.
func TestBlueish(t *testing.T) {
	c := colors256.NewColor(26)
	checkColor(t, "Blue-ish", c.TextTag(), "38;5;26")
	checkColor(t, "Blue-ish background", c.BackgroundTag(), "48;5;26")
}

// To test a purple-ish in 88/256bits colors.
func TestPurpleish(t *testing.T) {
	c := colors256.NewColor(91)
	checkColor(t, "Purple-ish", c.TextTag(), "38;5;91")
	checkColor(t, "Purple-ish background", c.BackgroundTag(), "48;5;91")
}

// To test a yellow-ish in 88/256bits colors.
func TestYellowish(t *testing.T) {
	c := colors256.NewColor(190)
	checkColor(t, "Yellow-ish", c.TextTag(), "38;5;190")
	checkColor(t, "Yellow-ish background", c.BackgroundTag(), "48;5;190")
}

// To test a red-ish in 88/256bits colors.
func TestRedish(t *testing.T) {
	c := colors256.NewColor(233)
	checkColor(t, "Red-ish", c.TextTag(), "38;5;233")
	checkColor(t, "Red-ish background", c.BackgroundTag(), "48;5;233")
}

// To test a gray-ish in 88/256bits colors.
func TestGrayish(t *testing.T) {
	c := colors256.NewColor(245)
	checkColor(t, "Gray-ish", c.TextTag(), "38;5;245")
	checkColor(t, "Gray-ish background", c.BackgroundTag(), "48;5;245")
}

// To check a color value and fail if necessary.
func checkColor(t *testing.T, name string, value string, ref string) {
	if value != ref {
		t.Errorf("%s color has wrong value: \"%s\" (!= \"%s\")", name, value, ref)
	}
}

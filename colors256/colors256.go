// Package colors256 provides functions to create a 88bits and 256bits color.
package colors256

import (
	"fmt"
	"gitlab.com/AeN_555/termcolor"
)

type (
	color struct {
		value uint8
	}
)

const (
	textTag       = "38;5;"
	backgroundTag = "48;5;"
)

// To create a new 88/256bits color.
func NewColor(newValue uint8) termcolor.Colorer {
	return &color{value: newValue}
}

// To convert a 88/256bits color to a tag string for text.
func (c *color) TextTag() string {
	return textTag + fmt.Sprint(c.value)
}

// To convert a 88/256bits color to a tag string for background.
func (c *color) BackgroundTag() string {
	return backgroundTag + fmt.Sprint(c.value)
}

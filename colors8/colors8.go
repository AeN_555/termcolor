// Package colors8 provides predefined color values for 8bits colors.
package colors8

import (
	"fmt"
	"gitlab.com/AeN_555/termcolor"
)

type (
	color struct {
		value uint8
	}
)

const (
	backgroundOffset = 10
)

var (
	Black     termcolor.Colorer = &color{value: 30}
	Red       termcolor.Colorer = &color{value: 31}
	Green     termcolor.Colorer = &color{value: 32}
	Yellow    termcolor.Colorer = &color{value: 33}
	Blue      termcolor.Colorer = &color{value: 34}
	Magenta   termcolor.Colorer = &color{value: 35}
	Cyan      termcolor.Colorer = &color{value: 36}
	LightGray termcolor.Colorer = &color{value: 37}
	Default   termcolor.Colorer = &color{value: 39}
)

// To convert a 8bits color to a tag string for text.
func (c *color) TextTag() string {
	return fmt.Sprint(c.value)
}

// To convert a 8bits color to a tag string for background.
func (c *color) BackgroundTag() string {
	return fmt.Sprint(backgroundOffset + c.value)
}

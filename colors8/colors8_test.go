package colors8_test

import (
	"gitlab.com/AeN_555/termcolor/colors8"
	"testing"
)

// To test the text color for every 8bits colors.
func TestTextColors(t *testing.T) {
	checkColor(t, "Black", colors8.Black.TextTag(), "30")
	checkColor(t, "Red", colors8.Red.TextTag(), "31")
	checkColor(t, "Green", colors8.Green.TextTag(), "32")
	checkColor(t, "Yellow", colors8.Yellow.TextTag(), "33")
	checkColor(t, "Blue", colors8.Blue.TextTag(), "34")
	checkColor(t, "Magenta", colors8.Magenta.TextTag(), "35")
	checkColor(t, "Cyan", colors8.Cyan.TextTag(), "36")
	checkColor(t, "LightGray", colors8.LightGray.TextTag(), "37")
	checkColor(t, "Default", colors8.Default.TextTag(), "39")
}

// To test the background color for every 8bits colors.
func TestBackgroundColors(t *testing.T) {
	checkColor(t, "Black background", colors8.Black.BackgroundTag(), "40")
	checkColor(t, "Red background", colors8.Red.BackgroundTag(), "41")
	checkColor(t, "Green background", colors8.Green.BackgroundTag(), "42")
	checkColor(t, "Yellow background", colors8.Yellow.BackgroundTag(), "43")
	checkColor(t, "Blue background", colors8.Blue.BackgroundTag(), "44")
	checkColor(t, "Magenta background", colors8.Magenta.BackgroundTag(), "45")
	checkColor(t, "Cyan background", colors8.Cyan.BackgroundTag(), "46")
	checkColor(t, "LightGray background", colors8.LightGray.BackgroundTag(), "47")
	checkColor(t, "Default background", colors8.Default.BackgroundTag(), "49")
}

// To check a color value and fail if necessary.
func checkColor(t *testing.T, name string, value string, ref string) {
	if value != ref {
		t.Errorf("%s color has wrong value: \"%s\" (!= \"%s\")", name, value, ref)
	}
}
